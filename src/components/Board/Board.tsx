import React, { MouseEvent } from 'react';
import { JeopardyService } from '../../api/service';
import Clue from '../Clue/Clue'
import './Board.css';

type Props = {};
type State = { 
  categories: any, 
  clues:any , 
  clueValues: Number[], 
  selectedClue:any, 
  selectedClues: any[], 
  dailyDoubleFound:boolean};

class Board extends React.Component<Props, State>{
  constructor(props:any){
    super(props); 
    this.state = {
      categories : [], 
      clues : [], 
      clueValues : [], 
      selectedClue : null,
      selectedClues : [],
      dailyDoubleFound : false
    }; 

    this.initGame = this.initGame.bind(this);
    this.done = this.done.bind(this);
    this.reset = this.reset.bind(this);
  }

  done() {
    this.setState({selectedClue: null});
  }

  componentDidMount(){
    this.initGame();
  }

  reset(){
    if (window.confirm("Are you sure you want to reset the game?")) { 
      this.state.selectedClue && this.done();
      this.initGame(this.state.categories[this.state.categories.length-1].id);
    }
  }

  initGame(lastCatId = null){
    let numCat = prompt("How many categories would you like?", "");
    let numClues = prompt("How many clues would you like?", "");

    if (numCat && numClues) {
      JeopardyService.fetchCategories(parseInt(numCat),lastCatId).then(resp=>{
        this.setState({categories: resp});
      }, err=>{
        alert(`Please try again. ${err}`);
      });
    
      JeopardyService.fetchClues().then(resp=>{
        let clues = resp.sort(function(a, b) {
          return a.value - b.value;
        });

        this.setState({clues: clues});

        let clueValues = this.state.clues.map((clue:any)=> clue.value);
        clueValues = clueValues.filter((item : Number, index: Number) => item && clueValues.indexOf(item) === index);
        clueValues = clueValues.slice(0,numClues);
        this.setState({clueValues : clueValues});
      }, err=>{
        alert(`Please try again. ${err}`);
      });

    } 
    else{
     this.initGame();
    }
  }

  loadClue(catId:Number,clueVal:Number){
    let selectedClues = this.state.selectedClues;
    selectedClues.push(`${catId}-${clueVal}`);
    this.setState({selectedClues: selectedClues });

    JeopardyService.fetchClue(catId, clueVal).then(clues=>{
      if(clues.length > 0){
        //daily double generator
        let dailyDouble = false;
        if(!this.state.dailyDoubleFound){
          dailyDouble = Boolean(Math.round(Math.random()));
          if(dailyDouble){
            this.setState({dailyDoubleFound:true});
          }
        }
        //select clue randomly from set of clues for category/value
        let selectedClue = clues[Math.floor(Math.random() * clues.length)];
        selectedClue.dailyDouble = dailyDouble;
        this.setState({selectedClue : selectedClue });
      }
      else{
        alert("No questions available for this Category/Clue combination.")
      }
    }, err=>{
      alert(`Please try again. ${err}`);
    });
  }


  render(){
    return ( 
      <div className="container-fluid justify-content-center">
         {!this.state.selectedClue && <table className="table table-responsive">
            <thead>
              <tr>
                {this.state.categories.map((cat:any) => <th>{cat.title}</th>)}
              </tr>
            </thead>
            <tbody>
              {this.state.clueValues.map((clueVal:any) => 
                <tr>
                  {this.state.categories.map((cat:any) => 
                  <td className={this.state.selectedClues.includes(`${cat.id}-${clueVal}`) ? "selected" : ""} 
                  onClick={(e) => !this.state.selectedClues.includes(`${cat.id}-${clueVal}`) && this.loadClue(cat.id, clueVal)}>{clueVal}</td>)}
                </tr>
              )}
            </tbody>
          </table>
        }
        {this.state.selectedClue && <Clue selectedClue={this.state.selectedClue} doneHandler={this.done}></Clue>}
        <button className="btn" onClick={this.reset}>Reset</button>
      </div>
    )
  }
}


export default Board;
