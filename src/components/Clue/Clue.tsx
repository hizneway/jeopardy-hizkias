import { Cluster } from 'cluster';
import React from 'react';
import './Clue.css';

type Props = {selectedClue : any, doneHandler: any};
type State = {showQuestion : boolean};

class Clue extends React.Component<Props, State>{

  constructor(props:any){
    super(props); 
    this.state = {
      showQuestion:true,
    }; 
  }

  render(){
    return ( 
      <div className="clue">
        {this.props.selectedClue.dailyDouble && <h1>Daily Double!</h1>}
        <h2>{this.state.showQuestion ? this.props.selectedClue.question : this.props.selectedClue.answer}</h2>
        <button className="btn btn-primary" onClick={(e) => this.setState({showQuestion : !this.state.showQuestion})}> 
          {this.state.showQuestion ? 'Show Answer': 'Show Question'}
        </button>
        <button className="btn" onClick={this.props.doneHandler}>Done</button>
      </div>
    );
  }
}

export default Clue;


