import React from 'react';
import ReactDOM from 'react-dom';
import Clue from './Clue';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Clue />, div);
  ReactDOM.unmountComponentAtNode(div);
});