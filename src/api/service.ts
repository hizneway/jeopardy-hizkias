
const baseURL = 'https://jservice.io/api/';

const fetchCategories = (numCat:Number, lastCatId = null): Promise<any[]> => {
  const categoriesURL = lastCatId ? `${baseURL}/categories?count=${numCat}&offset=${lastCatId}` : `${baseURL}/categories?count=${numCat}`;
  return fetch(categoriesURL)
    .then((response) => (response.json()));
};

const fetchClues = (): Promise<any[]> => {
    const cluesURL = `${baseURL}/clues`;

    return fetch(cluesURL)
        .then((response) => (response.json()));
};

const fetchClue = (catId:Number, catValue:Number): Promise<any> => {
    const cluesURL = `${baseURL}/clues?value=${catValue}&category=${catId}`;

    return fetch(cluesURL)
        .then((response) => (response.json()));
};
  

export const JeopardyService = {
  fetchCategories,
  fetchClues,
  fetchClue
};